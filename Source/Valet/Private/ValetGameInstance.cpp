﻿// Copyright Denis Xalagor Petrov. MIT licensed.


#include "ValetGameInstance.h"

#include "GameFramework/GameUserSettings.h"
#include "Kismet/GameplayStatics.h"

void UValetGameInstance::Init()
{
	Super::Init();

	GEngine->GameUserSettings->SetVSyncEnabled(true);
	GEngine->GameUserSettings->SetFullscreenMode(EWindowMode::WindowedFullscreen);
	GEngine->GameUserSettings->ApplySettings(true);
	GEngine->GameUserSettings->SaveSettings();
	GEngine->Exec(GetWorld(), TEXT("t.maxFPS 60"));
}

void UValetGameInstance::DestroyActors(TArray<TSubclassOf<AActor>> ActorClasses) const
{
	UGameplayStatics::SetGamePaused(GetWorld(), true);
	TArray<AActor*> FoundActors;
	for (const TSubclassOf<AActor> ActorClass : ActorClasses)
	{
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ActorClass, FoundActors);
		for (AActor* FoundActor : FoundActors)
		{
			FoundActor->Destroy();
		}
	}
	UGameplayStatics::SetGamePaused(GetWorld(), true);
}
