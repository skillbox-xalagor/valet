// Copyright Epic Games, Inc. All Rights Reserved.

#include "ValetGameMode.h"

#include "Card.h"
#include "Deck.h"
#include "ValetGameInstance.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "VisualLogger/VisualLogger.h"

void AValetGameMode::BeginPlay()
{
	Super::BeginPlay();

	// Destroy old Cards and Decks
	const UValetGameInstance* MyGameInstance = Cast<UValetGameInstance>(GetGameInstance());
	const TArray<TSubclassOf<AActor>> ActorsToDestroy
	{
		ACard::StaticClass(),
		ADeck::StaticClass()
	};
	MyGameInstance->DestroyActors(ActorsToDestroy);
	PlayerHand.Empty();
	DealerHand.Empty();
	MyDeck = GetWorld()->SpawnActor<ADeck>(ADeck::StaticClass());
	APlayerController* MyPlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	UGameWidget* MyGameWidget = CreateWidget<UGameWidget>(MyPlayerController, GameWidgetClass);
	MyGameWidget->MyGameMode = this;
	MyGameWidget->BindEvents();
	FInputModeUIOnly MyInputMode;
	MyInputMode.SetLockMouseToViewportBehavior(EMouseLockMode::LockInFullscreen);
	MyPlayerController->SetShowMouseCursor(true);
	MyPlayerController->SetInputMode(MyInputMode);
	MyGameWidget->AddToViewport();
}

void AValetGameMode::StartGame()
{
	// Prepare Cards
	if (IsValid(MyDeck))
	{
		MyDeck->FillDeck();
		MyDeck->Shuffle();
	}
	MyDeck->MoveFirstCards(DealerHand, 1);
	MyDeck->MoveFirstCards(PlayerHand, 2);
	const FEndGameState MyWinStateCheckResult = CheckEndGameState(false);
	if (!MyWinStateCheckResult.bIsGameOver)
	{
		DealerTake();
		CheckEndGameState(false);
	}
}

void AValetGameMode::Hit()
{
	MyDeck->MoveFirstCards(PlayerHand, 1);
	CheckEndGameState(false);
}

FEndGameState AValetGameMode::CheckEndGameState(const bool bDoesPlayerPass)
{
	const int32 PlayerPoints = GetCardPoints(true);
	UE_VLOG(this, LogTemp, Log, TEXT("Player points: %d"), PlayerPoints);
	const int32 DealerPoints = GetCardPoints(false);
	UE_VLOG(this, LogTemp, Log, TEXT("Dealer points: %d"), DealerPoints);

	FEndGameState MyEndGameState;
	MyEndGameState.bIsGameOver = true;
	MyEndGameState.WinState = EWinState::PlayerWins;

	// If Player wins
	const bool PlayerHas21 = PlayerPoints == 21;
	if (PlayerHas21)
	{
		UE_VLOG(this, LogTemp, Log, TEXT("Player Has 21"));
	}
	const bool DealerOvertakes = DealerPoints > 21;
	if (DealerOvertakes)
	{
		UE_VLOG(this, LogTemp, Log, TEXT("Dealer Overtakes"));
	}
	const bool PlayerWinsByPoints = PlayerPoints > DealerPoints && (bDoesPlayerPass || DealerPoints >= 16);
	if (PlayerWinsByPoints)
	{
		UE_VLOG(this, LogTemp, Log, TEXT("Player Wins by Points"));
	}
	const bool bPlayerOvertakes = PlayerPoints > 21;
	if (bPlayerOvertakes)
	{
		UE_VLOG(this, LogTemp, Log, TEXT("Player Overtakes"));
	}
	const bool bDoesPlayerWins =
		PlayerHas21
		|| DealerOvertakes
		|| PlayerWinsByPoints && !bPlayerOvertakes;
	if (bDoesPlayerWins)
	{
		OnGameOverDelegate.Broadcast(EWinState::PlayerWins);
		MyEndGameState.WinState = EWinState::PlayerWins;
		return MyEndGameState;
	}

	// If Dealer wins
	const bool bDealerHas21 = DealerPoints == 21;
	if (bDealerHas21)
	{
		UE_VLOG(this, LogTemp, Log, TEXT("Dealer Has 21"));
	}
	const bool bDealerWinsByPoints = PlayerPoints < DealerPoints && bDoesPlayerPass;
	if (bDealerWinsByPoints)
	{
		UE_VLOG(this, LogTemp, Log, TEXT("Dealer Wins by Points"));
	}
	const bool bDoesDealerWins =
		bPlayerOvertakes ||
		bDealerHas21 ||
		bDealerWinsByPoints;
	if (bDoesDealerWins)
	{
		OnGameOverDelegate.Broadcast(EWinState::DealerWins);
		MyEndGameState.WinState = EWinState::DealerWins;
		return MyEndGameState;
	}

	// If it's tie
	const bool bIsTie =
		PlayerPoints == DealerPoints && DealerPoints >= 16;
	if (bIsTie)
	{
		UE_VLOG(this, LogTemp, Log, TEXT("Tie"));
		OnGameOverDelegate.Broadcast(EWinState::Tie);
		MyEndGameState.WinState = EWinState::Tie;
		return MyEndGameState;
	}

	MyEndGameState.bIsGameOver = false;
	return MyEndGameState;
}

int32 AValetGameMode::GetCardPoints(const bool bIsPlayerHand) const
{
	TArray<ACard*> MyCards = bIsPlayerHand ? PlayerHand : DealerHand;

	if (MyCards.Num() == 0)
	{
		return 0;
	}

	bool bCheckAceBonus = false;
	int32 Points = 0;
	for (const ACard* Card : MyCards)
	{
		if (Card->Rank == ERank::Ace)
		{
			bCheckAceBonus = true;
		}

		int32 PointsToAdd = 0;
		switch (Card->Rank)
		{
		case ERank::Ace:
			PointsToAdd = 1;
			break;
		case ERank::Two:
			PointsToAdd = 2;
			break;
		case ERank::Three:
			PointsToAdd = 3;
			break;
		case ERank::Four:
			PointsToAdd = 4;
			break;
		case ERank::Five:
			PointsToAdd = 5;
			break;
		case ERank::Six:
			PointsToAdd = 6;
			break;
		case ERank::Seven:
			PointsToAdd = 7;
			break;
		case ERank::Eight:
			PointsToAdd = 8;
			break;
		case ERank::Nine:
			PointsToAdd = 9;
			break;
		case ERank::Ten:
			PointsToAdd = 10;
			break;
		case ERank::Jack:
			PointsToAdd = 10;
			break;
		case ERank::Queen:
			PointsToAdd = 10;
			break;
		case ERank::King:
			PointsToAdd = 10;
			break;
		}
		Points += PointsToAdd;
	}
	return (bCheckAceBonus && Points + 10 <= 21) ? Points + 10 : Points;
}

void AValetGameMode::DealerTake()
{
	while (GetCardPoints(false) <= 16)
	{
		MyDeck->MoveFirstCards(DealerHand, 1);
	}
}

void AValetGameMode::PlayerPasses()
{
	DealerTake();
	CheckEndGameState(true);
}
