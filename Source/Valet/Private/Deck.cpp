﻿// Copyright Denis Xalagor Petrov. MIT licensed.

#include "Deck.h"
#include "Card.h"
#include "VisualLogger/VisualLogger.h"

// Sets default values
ADeck::ADeck()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ADeck::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ADeck::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ADeck::Shuffle()
{
	// Initialize random stream using Cycles() 
	const uint32 Cycles = FPlatformTime::Cycles();
	FRandomStream RandomStream(*(int32*)&Cycles);
	// Shuffle the list
	const int32 NumShuffles = Cards.Num() - 1;
	for (int32 i = 0; i < NumShuffles; ++i)
	{
		int32 SwapIdx = RandomStream.RandRange(i, NumShuffles);
		Cards.Swap(i, SwapIdx);
	}
}

void ADeck::MoveFirstCards(TArray<ACard*>& ToHand, int Count)
{
	if (Cards.Num() == 0)
	{
		return;
	}
	for (int32 i = 0; i < Count; ++i)
	{
		ToHand.Add(Cards[0]);
		Cards.RemoveAt(0);
	}
	OnCardMoveDelegate.Broadcast();
}

void ADeck::FillDeck()
{
	TArray<ACard*> NewDeck;
	for (int Suit = 0; Suit < static_cast<int>(ESuit::Spades) + 1; ++Suit)
	{
		for (int Rank = 0; Rank < static_cast<int>(ERank::King) + 1; ++Rank)
		{
			FActorSpawnParameters SpawnInfo;
			UWorld* MyWorld = GEngine->GameViewport->GetWorld();
			ACard* NewCard = MyWorld->SpawnActor<ACard>(SpawnInfo);
			NewCard->Rank = static_cast<ERank>(Rank);
			NewCard->Suit = static_cast<ESuit>(Suit);
			NewDeck.Add(NewCard);
			Cards = NewDeck;
			UE_VLOG(this, LogTemp, Log, TEXT("Added card to deck: %s, %s"),
			        *UEnum::GetValueAsString<ERank>(NewCard->Rank),
			        *UEnum::GetValueAsString<ESuit>(NewCard->Suit)
			);
		}
	}
}
