﻿// Copyright Denis Xalagor Petrov. MIT licensed.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "ValetGameInstance.generated.h"

/**
 * GameInstance with optimizations
 */
UCLASS()
class VALET_API UValetGameInstance : public UGameInstance
{
	GENERATED_BODY()

	virtual void Init() override;

public:
	UFUNCTION(BlueprintCallable)
	void DestroyActors(TArray<TSubclassOf<AActor>> ActorClasses) const;
};
