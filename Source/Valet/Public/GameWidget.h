// Copyright Denis Xalagor Petrov. MIT licensed.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameWidget.generated.h"

class AValetGameMode;
/**
 * 
 */
UCLASS()
class VALET_API UGameWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
	void BindEvents();
	UPROPERTY(BlueprintReadOnly)
	AValetGameMode* MyGameMode;
};
