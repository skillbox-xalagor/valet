﻿// Copyright Denis Xalagor Petrov. MIT licensed.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Deck.generated.h"

class ACard;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCardMoveSignature);

UCLASS()
class VALET_API ADeck : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ADeck();
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
	void Shuffle();
	UFUNCTION()
	void MoveFirstCards(TArray<ACard*>& ToHand, int Count);
	UPROPERTY(BlueprintReadWrite)
	TArray<ACard*> Cards;
	UFUNCTION(BlueprintCallable)
	void FillDeck();
	UPROPERTY(BlueprintAssignable)
	FOnCardMoveSignature OnCardMoveDelegate;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
