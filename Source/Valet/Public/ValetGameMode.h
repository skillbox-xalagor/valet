// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Deck.h"
#include "GameFramework/GameMode.h"
#include "GameWidget.h"
#include "ValetGameMode.generated.h"

class ACard;

UENUM(BlueprintType)
enum class EWinState : uint8
{
	PlayerWins UMETA(DisplayName="Player wins"),
	DealerWins UMETA(DisplayName="Dealer wins"),
	Tie UMETA(DisplayName="Tie"),
};

USTRUCT(BlueprintType)
struct FEndGameState
{
	GENERATED_BODY()

	UPROPERTY()
	EWinState WinState;
	UPROPERTY()
	bool bIsGameOver;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGameOverSignature, EWinState, WinState);

/**
 * Main Game Mode
 */
UCLASS()
class VALET_API AValetGameMode : public AGameMode
{
	GENERATED_BODY()
	virtual void BeginPlay() override;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UGameWidget> GameWidgetClass = UGameWidget::StaticClass();

public:
	UFUNCTION(BlueprintCallable)
	void StartGame();
	UFUNCTION(BlueprintCallable)
	void Hit();
	UFUNCTION(BlueprintCallable)
	FEndGameState CheckEndGameState(bool bDoesPlayerPass);
	UFUNCTION(BlueprintCallable)
	int32 GetCardPoints(bool bIsPlayerHand) const;
	UFUNCTION(BlueprintCallable)
	void DealerTake();
	UFUNCTION(BlueprintCallable)
	void PlayerPasses();
	UPROPERTY(BlueprintAssignable)
	FOnGameOverSignature OnGameOverDelegate;
	UPROPERTY(BlueprintReadOnly)
	TArray<ACard*> PlayerHand;
	UPROPERTY(BlueprintReadOnly)
	TArray<ACard*> DealerHand;
	UPROPERTY(BlueprintReadWrite)
	ADeck* MyDeck;
};
