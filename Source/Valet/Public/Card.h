﻿// Copyright Denis Xalagor Petrov. MIT licensed.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Card.generated.h"

UENUM(BlueprintType)
enum class ESuit : uint8
{
	Hearts,
	Diamonds,
	Clubs,
	Spades
};

UENUM(BlueprintType)
enum class ERank : uint8
{
	Ace,
	Two,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King
};

UCLASS()
class VALET_API ACard : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACard();

	UPROPERTY(BlueprintReadWrite)
	ESuit Suit = ESuit::Hearts;
	UPROPERTY(BlueprintReadWrite)
	ERank Rank = ERank::Ace;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
