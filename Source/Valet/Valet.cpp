// Copyright Epic Games, Inc. All Rights Reserved.

#include "Valet.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, Valet, "Valet");
